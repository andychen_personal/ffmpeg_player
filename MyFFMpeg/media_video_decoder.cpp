#include "stdafx.h"
#include "media_video_decoder.h"
#include "SDLRender.h"

#ifdef _WIN32
//Windows
extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
};
#else
//Linux...
#ifdef __cplusplus
extern "C"
{
#endif
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <sdl2/SDL.h>
#ifdef __cplusplus
};
#endif
#endif

static AVFormatContext *pFormatCtx = NULL;
static AVCodecContext *pCodecCtx = NULL;
static AVCodec *pCodec = NULL;
static AVFrame *pAVFrame = NULL, *pAVFrameYuv = NULL;
static SwsContext *pSwsContext;
FILE *pFileSaveYuv = NULL;
static bool m_isStart = false;

static SDLRender m_SDLRender;


static int openFile()
{
    #if OUTPUT_YUV420P 
        pFileSaveYuv = fopen("output_yuv420p.yuv", "wb+");
    #endif
    return 0;
}

int video_decoder_init(const char *filePath)
{
    int mediaTypeIndex = -1;
    unsigned int i = 0;
    AVStream *pStream = NULL;

    openFile();

    // step0: register
    av_register_all();

    avformat_network_init();

    //step1: get the avformat context;
    pFormatCtx = avformat_alloc_context();

    if (filePath == NULL)
    {
        filePath = FILE_PATH_VIDEO;
    }
    printf("preare for OPEN file %s \n",filePath);
    //step2: open a file 
    if (avformat_open_input(&pFormatCtx, filePath, NULL, NULL) != 0)
    {
        printf("OPEN file failed \n");
        return -1;
    }
    //step3: find the stream info
    if (avformat_find_stream_info(pFormatCtx, NULL) < 0)
    {
        printf("find file stream failed");
        return -1;
    }
    //step4: get the stream type
    for (i = 0; i < pFormatCtx->nb_streams; i++)
    {
        if (pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            mediaTypeIndex = i;
            break;
        }
    }

    if (mediaTypeIndex == -1)
    {
        printf("can't find the video stream");
        return -1;
    }
    //step5: find the decoder
    pStream = pFormatCtx->streams[mediaTypeIndex];
    pCodecCtx = pStream->codec;
    pCodec = avcodec_find_decoder(pCodecCtx->codec_id);

    
    //init sdl
    m_SDLRender.init(pCodecCtx->width, pCodecCtx->height);
    return 0;


}

int video_decoder_decoder()
{
    uint8_t *out_buffer;
    AVPacket *pAVPacket;
  
    int intGotFrame, ret;
    int videoSize;
    if (pCodec == NULL)
    {
        return -1;
    }

    //step6: open decoder
    if (avcodec_open2(pCodecCtx, pCodec, NULL) != 0) {
        printf("can't open decoder");
        return -1;
    }
    //step7: malloc the save data buffer
    pAVFrame = av_frame_alloc();
    pAVFrameYuv = av_frame_alloc();
    pAVPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
    out_buffer = (uint8_t *)av_malloc(avpicture_get_size(PIX_FMT_YUV420P, pCodecCtx->width, pCodecCtx->height));
    avpicture_fill((AVPicture *)pAVFrameYuv, out_buffer, PIX_FMT_YUV420P, pCodecCtx->width, pCodecCtx->height);

    //get the wscale context
    pSwsContext = sws_getContext(pCodecCtx->width, pCodecCtx->height, pCodecCtx->pix_fmt, pCodecCtx->width,
        pCodecCtx->height, PIX_FMT_YUV420P, SWS_BICUBIC, NULL, NULL, NULL);


    //step8 read frame 
    while (av_read_frame(pFormatCtx, pAVPacket) == 0)
    {
        //get type stream data
        if (pAVPacket->stream_index == AVMEDIA_TYPE_VIDEO)
        {
            //FOR VIDEO
            //step9: todo decoder a frame
            ret = avcodec_decode_video2(pCodecCtx, pAVFrame, &intGotFrame, pAVPacket);
            if (ret < 0)
            {
                printf("decode video error return \n");
                return -1;
            }
            if (intGotFrame)
            {
                sws_scale(pSwsContext, pAVFrame->data, pAVFrame->linesize, 0, pCodecCtx->height, pAVFrameYuv->data, pAVFrameYuv->linesize);

                if (pFileSaveYuv)
                {
                    //printf("output yuv data to local file BEGIN");
                    //output the yuv data to file
                    videoSize = pCodecCtx->width * pCodecCtx->height;
                    fwrite(pAVFrameYuv->data[0], 1, videoSize,pFileSaveYuv);//y
                    fwrite(pAVFrameYuv->data[1], 1, videoSize/4, pFileSaveYuv);//u
                    fwrite(pAVFrameYuv->data[2], 1, videoSize/4, pFileSaveYuv);//v
                }

                //push the YUV data to render
                ret = m_SDLRender.render((void *const *)pAVFrameYuv->data, pAVFrameYuv->linesize);
                if (ret == -1)
                {
                    printf("render video exit return \n");
                    return -1;
                }
              
            }

        }
        else
        {
            //FOR AUDIO
        }

        //free
        av_free_packet(pAVPacket);
    }

    return 0;

}

void video_decoder_fini()
{
    printf("decoder fini \n");
    sws_freeContext(pSwsContext);
    if (pFileSaveYuv)
    {
        fclose(pFileSaveYuv);
        pFileSaveYuv = NULL;
    }
    av_frame_free(&pAVFrame);
    av_frame_free(&pAVFrameYuv);
    avcodec_close(pCodecCtx);
    avformat_close_input(&pFormatCtx);
    
    m_SDLRender.fini();
    
}