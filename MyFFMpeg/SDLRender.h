#pragma once
#ifdef _WIN32
//Windows
extern "C"
{
#include "sdl2/SDL.h"
};
#else
//Linux...
#ifdef __cplusplus
extern "C"
{
#endif
#include <sdl2/SDL.h>
#ifdef __cplusplus
};
#endif
#endif
class SDLRender
{
private:
    SDL_Window *pSdlWindown = NULL;
    SDL_Renderer *pSdlRender = NULL;
    SDL_Texture *pSdlTexture = NULL;
    SDL_Thread *pSdlThread = NULL;
    SDL_Rect srcRect,destRect;
    bool m_isInited = false;
    int screen_w, screen_h;

public:
    SDLRender();
    ~SDLRender();
    int init(int width, int height);
    int render(void* const yuvData[], int* linesize);
    int fini();
};

