/* FILE NAME: _MEDIA_VIDEO_DECODER_.h
* Version: 1.0,  Date: xxxx-xx-xx
* Description: xxxxx
* Platform: xxxxx
*
***************************************************************************/
#ifndef  _MEDIA_VIDEO_DECODER_
#define _MEDIA_VIDEO_DECODER_

#include <stdio.h>
#define __STDC_CONSTANT_MACROS

#ifdef _WIN32
//Windows
extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"

    //Output YUV420P data as a file 
    #define OUTPUT_YUV420P 1
    #define FILE_PATH_VIDEO "test_video_480x272.h264"  //"testEncode.h264"; 
    #define FILE_PATH_VIDEO_OUTPUT_YUV420P "output_yuv420p.yuv"
};
#else
//Linux...
#ifdef __cplusplus
extern "C"
{
#endif
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <SDL2/SDL.h>
#ifdef __cplusplus
    //Output YUV420P data as a file 
#define OUTPUT_YUV420P 0
};
#endif
#endif

    int video_decoder_init(const char *filePath);
    int video_decoder_decoder();
    void video_decoder_fini();



#endif // ! _MEDIA_VIDEO_DECODER_