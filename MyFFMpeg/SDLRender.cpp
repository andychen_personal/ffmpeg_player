#include "stdafx.h"
#include "SDLRender.h"

bool m_thread_exit = false;
bool m_thread_pause = false;
#define REFRESH_EVENT  (SDL_USEREVENT + 1)
#define EXIT_EVENT  (SDL_USEREVENT + 2)

SDLRender::SDLRender()
{

}


SDLRender::~SDLRender()
{

}

int m_sdl_render_EventsThread(void *data)
{
    while (!m_thread_exit)
    {
        if (!m_thread_pause)
        {
            SDL_Event events;
            events.type = REFRESH_EVENT;
            SDL_PushEvent(&events);
        }
        
        SDL_Delay(40);
    }
    m_thread_exit = true;
    //Break
    SDL_Event event;
    event.type = EXIT_EVENT;
    SDL_PushEvent(&event);

    return 0;

}

int SDLRender::init(int width, int height)
{
    screen_w = width;
    screen_h = height;
    printf("initialize SDL screen size[%d,%d]\n", screen_w,screen_h);
    //init
    if (SDL_Init(SDL_INIT_VIDEO)) {
        printf("Could not initialize SDL - %s\n", SDL_GetError());
        return -1;
    }
    //create window
    pSdlWindown = SDL_CreateWindow("Andy's FFMEPG Player", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, screen_w, screen_h, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
    if (!pSdlWindown)
    {
        printf("Could not createWindow SDL - %s\n", SDL_GetError());
        return -1;
    }
    //create render
    pSdlRender = SDL_CreateRenderer(pSdlWindown, -1, 0);
    if (!pSdlRender)
    {
        printf("Could not createRender SDL - %s\n", SDL_GetError());
        return -1;
    }
    //create texture
    pSdlTexture = SDL_CreateTexture(pSdlRender, SDL_PIXELFORMAT_IYUV, SDL_TEXTUREACCESS_STREAMING, screen_w, screen_h);
    if (!pSdlTexture)
    {
        printf("Could not create SDL_CreateTexture SDL - %s\n", SDL_GetError());
        return -1;
    }

    //create rect 原始图像大小
    srcRect.x = 0;
    srcRect.y = 0;
    srcRect.w = screen_w;
    srcRect.h = screen_h;

    //目标图像大小【调整窗口大小缩放】
    destRect = srcRect;

    pSdlThread = SDL_CreateThread(m_sdl_render_EventsThread, NULL, NULL);
    m_isInited = true;


    return 0;

}

int SDLRender::fini()
{
    m_isInited = false;
    SDL_Quit();

    pSdlWindown = NULL;
    pSdlRender = NULL;
    pSdlTexture = NULL;
    return 0;
}

/*
[Loop to Render data]
*/
int SDLRender::render(void *const yuvData[], int* linesize)
{
    if (!m_isInited)
    {
        return -1;
    }

    //loop in listener the sync event
    SDL_Event event;
    SDL_WaitEvent(&event);

    if (event.type == REFRESH_EVENT)
    {
        //Fixed resize window 
        if (destRect.w != screen_w || destRect.h != screen_h)
        {
            destRect.x = 0;
            destRect.y = 0;
            destRect.w = screen_w;
            destRect.h = screen_h;
        }


        //to-do render refersh
        int ret = SDL_UpdateYUVTexture(pSdlTexture, &srcRect,
            (uint8_t *const)yuvData[0], linesize[0],
            (uint8_t *const)yuvData[1], linesize[1],
            (uint8_t *const)yuvData[2], linesize[2]);
            

        if (ret == -1)
        {
            return -1;
        }
        SDL_RenderClear(pSdlRender);

        /*
        renderer：渲染目标。
        texture：输入纹理。
        srcrect：选择输入纹理的一块矩形区域作为输入。设置为NULL的时候整个纹理作为输入。
        dstrect：选择渲染目标的一块矩形区域作为输出。设置为NULL的时候整个渲染目标作为输出。
        成功的话返回0，失败的话返回-1。*/
        SDL_RenderCopy(pSdlRender, pSdlTexture, &srcRect, &destRect);

        SDL_RenderPresent(pSdlRender);
        return 0;
    }
    else if (event.type == SDL_WINDOWEVENT)
    {
        //If Resize adjust the window size
       SDL_GetWindowSize(pSdlWindown, &screen_w, &screen_h);
       printf("SDL_GetWindowSize [%d,%d] \n", screen_w,screen_h);
    }
    else if (event.type == SDL_KEYDOWN)
    {
        if (event.key.keysym.sym == SDLK_SPACE)
        {
            m_thread_pause = !m_thread_pause;
        }
    }
    else if (event.type == SDL_QUIT)
    {
        m_thread_exit = true;
    }
    else if (event.type == EXIT_EVENT)
    {
        return -1;
    }

    /* can't do the delay event on here, it will be blocked the window move*/
    // SDL_Delay(10);
    return 0;

}