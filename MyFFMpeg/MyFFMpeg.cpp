// MyFFMpeg.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "media_video_decoder.h"
#define __STDC_CONSTANT_MACROS
extern "C"
{
#include "libavcodec/avcodec.h "
}
/*
Exp: command: MyFFMpeg -f testEncode.h264
*/
int main(int argc, char* argv[])
{
    int ret = -1;
    char* filePath = NULL;
    char params[128] = {0};

    printf("%s\n", avcodec_configuration());
   // system("pause");
    int argsLen = argc;
    printf(">>>>>> argc = %d, argsLen = %d\n",argc, argsLen);
    if (argsLen > 1) {
        strcpy(params, argv[1]);
        printf(">>>>>> params[1] = %s\n", params);
        if (strcmp(params,"-f") == 0)
        {
            if (argsLen > 2)
            {
                printf(">>>>>> params[2] = %s\n",argv[2]);
                filePath = (char*) malloc(sizeof(char) * strlen(argv[2]) + 1);
                memset(filePath,0, strlen(argv[2]) + 1);
                memcpy(filePath, argv[2], strlen(argv[2]));

                printf(">>>>> filePath = %s, strlen = %d, FILE_PATH_VIDEO = %s, strlen = %d \n", filePath, strlen(filePath), FILE_PATH_VIDEO, strlen(FILE_PATH_VIDEO));
                printf(">>>>>  strcmp(filePath, FILE_PATH_VIDEO) ? %d\n", strcmp(filePath, FILE_PATH_VIDEO));

            }
        }
    }

    ret = video_decoder_init(filePath);
    printf("init %d\n", ret);
    if (ret == 0)
    {
        ret = video_decoder_decoder();
        printf("decoder %d\n", ret);
        if (ret == -1) {
            printf("decoder exit \n");
        }
    }
    free(filePath);
   // system("pause");
    video_decoder_fini();
	return 0;
}

